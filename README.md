# PHP Contact Form

This is a simple contact form application built in Laravel. Input is validated, and then the message is both e-mailed to the recipient and stored in a MySQL database.

## Installing & Running

1. Clone this repository.
2. Run `composer install` within the project directory.
3. Copy the `.env.example` file to `.env`.
4. Ensure the `.env` file includes the credentials for your MySQL database and the configuration for your SMTP server. You can also change the `MAIL_MAILER` variable to `log`, in which case the outgoing e-mails will simply be logged to `storage/logs/laravel.log` rather than being submitted to an SMTP server.
5. Create the `contact_form` schema in MySQL (you can use the `database/create_schema.sql` file if you want).
6. Run migrations using `php artisan migrate`.
7. Run tests using either `php artisan test` or `vendor/bin/phpunit`.
8. Run the dev server using either `php artisan serve` or `php -S 127.0.0.1:8000 -t public`.

## Technologies Used

* PHP (tested with 8.0; should be compatible with 7.3 and up)
* Laravel 8.18.1
* MySQL (tested with 8.0.22; should be compatible with 5.6 and up)
* Composer 2.0.8
* PHPUnit 9.5

## Key Files
* `app/Http/Controllers/ContactForm.php` - The controller that handles form submissions
* `app/Http/Requests/SendFormRequest.php` - A custom request class where the form validation rules are defined
* `app/Mail/MessageReceived.php` - A mailable class that defines the e-mail message that is sent upon a successful form submission
* `app/Models/Message.php` - The model class (using the Eloquent ORM) for the `messages` database table
* `database/migrations/2020_12_11_224648_create_messages_table.php` - The migration class that defines the `messages` table and addes the necessary columns
* `database/create_schema.sql` - A simple script to create the necessary database schema
* `public/css/*`, `public/img/*`, `public/js/*`, and `public/vendor/*` - The CSS, image, and JavaScript assets provided by Dealer Inspire
* `resources/views/emails/message_received.blade.php` - A Blade template for the HTML e-mail that is sent upon a successful form submission
* `resources/views/index.blade.php` - The Blade form template, incorporating the HTML template that was provided by Dealer Inspire
* `routes/web.php` - Route definitions for the HTML form page and the form submission action
* `tests/Feature/ContactFormTest.php` - A PHPUnit test suite for the contact form
* `.env.example` - the example environment configuration file