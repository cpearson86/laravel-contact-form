<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendFormRequest;
use App\Mail\MessageReceived;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactForm extends Controller
{
    public function send(SendFormRequest $request)
    {
        $message = new Message;
        $message->full_name = $request->fullName;
        $message->email = $request->email;
        if (isset($request->phone))
        {
            $message->phone = $request->phone;
        }
        $message->content = $request->message;
        $message->save();

        Mail::to(env('MESSAGE_RECIPIENT'))->send(new MessageReceived($message));

        return redirect()->route('index')->with('status', 'Your message was sent successfully!');
    }
}
