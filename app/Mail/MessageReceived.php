<?php

namespace App\Mail;

use App\Models\Message as MessageModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessageReceived extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * An instance of the Message model
     * 
     * @var \App\Models\Message
     */
    public $messageModel;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(MessageModel $messageModel)
    {
        $this->messageModel = $messageModel;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.message_received');
    }
}
