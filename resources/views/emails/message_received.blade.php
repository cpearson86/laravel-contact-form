<html>
    <head>
        <title>Message Received</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <h3>You have received a new message!</h3>
            <dl class="row">
                <dt class="col-sm-2">From:</dt>
                <dd class="col-sm-10">{{ $messageModel->full_name }}</dd>

                <dt class="col-sm-2">E-Mail Address:</dt>
                <dd class="col-sm-10">{{ $messageModel->email }}</dd>

                @if ($messageModel->phone)
                <dt class="col-sm-2">Phone Number:</dt>
                <dd class="col-sm-10">{{ $messageModel->phone }}</dd>
                @endif

                <dt class="col-sm-2">Message:</dt>
                <dd class="col-sm-10">{{ $messageModel->content }}</dd>
            </dl>
        </div>
    </body>
</html>