<?php

namespace Tests\Feature;

use App\Mail\MessageReceived;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class ContactFormTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        Mail::fake();
    }

    protected function getExampleMessageData(): array
    {
        return [
            'fullName' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->numerify('###-###-####'),
            'message' => $this->faker->text
        ];
    }

    protected function mailAssertion(array $exampleMessage)
    {
        Mail::assertSent(MessageReceived::class, function (MessageReceived $mail) use ($exampleMessage) {
            return $mail->hasTo(env('MESSAGE_RECIPIENT')) &&
                    $mail->messageModel->full_name == $exampleMessage['fullName'] &&
                    $mail->messageModel->email == $exampleMessage['email'] &&
                    $mail->messageModel->phone == $exampleMessage['phone'] &&
                    $mail->messageModel->content == $exampleMessage['message'];
        });
    }

    protected function dbAssertion(array $exampleMessage)
    {
        $this->assertDatabaseHas('messages', [
            'full_name' => $exampleMessage['fullName'],
            'email' => $exampleMessage['email'],
            'phone' => $exampleMessage['phone'],
            'content' => $exampleMessage['message']
        ]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testFormPage()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertViewIs('index');
    }

    public function testWithPhone()
    {
        $exampleMessage = $this->getExampleMessageData();
        $response = $this->post('/send', $exampleMessage);

        $response->assertSessionHasNoErrors();
        $response->assertSessionHas('status');
        $response->assertRedirect('/');

        $this->mailAssertion($exampleMessage);
        $this->dbAssertion($exampleMessage);
    }

    public function testWithoutPhone()
    {
        $exampleMessage = $this->getExampleMessageData();
        $exampleMessage['phone'] = null;

        $response = $this->post('/send', $exampleMessage);

        $response->assertSessionHasNoErrors();
        $response->assertSessionHas('status');
        $response->assertRedirect('/');

        $this->mailAssertion($exampleMessage);
        $this->dbAssertion($exampleMessage);
    }

    public function testWithoutName()
    {
        $exampleMessage = $this->getExampleMessageData();
        $exampleMessage['fullName'] = null;

        $response = $this->post('/send', $exampleMessage);

        $response->assertSessionHasErrors(['fullName']);
        $response->assertSessionMissing('status');
        $response->assertRedirect('/');

        Mail::assertNotSent(MessageReceived::class);
        $this->assertDatabaseMissing('messages', ['email' => $exampleMessage['email']]);
    }

    public function testWithoutEmail()
    {
        $exampleMessage = $this->getExampleMessageData();
        $exampleMessage['email'] = null;

        $response = $this->post('/send', $exampleMessage);

        $response->assertSessionHasErrors(['email']);
        $response->assertSessionMissing('status');
        $response->assertRedirect('/');

        Mail::assertNotSent(MessageReceived::class);
        $this->assertDatabaseMissing('messages', ['full_name' => $exampleMessage['fullName']]);
    }

    public function testInvalidEmail()
    {
        $exampleMessage = $this->getExampleMessageData();
        $exampleMessage['email'] = $this->faker->word;

        $response = $this->post('/send', $exampleMessage);

        $response->assertSessionHasErrors(['email']);
        $response->assertSessionMissing('status');
        $response->assertRedirect('/');

        Mail::assertNotSent(MessageReceived::class);
        $this->assertDatabaseMissing('messages', ['email' => $exampleMessage['email']]);
    }

    public function testInvalidPhone()
    {
        $exampleMessage = $this->getExampleMessageData();
        $exampleMessage['phone'] = $this->faker->word;

        $response = $this->post('/send', $exampleMessage);

        $response->assertSessionHasErrors(['phone']);
        $response->assertSessionMissing('status');
        $response->assertRedirect('/');

        Mail::assertNotSent(MessageReceived::class);
        $this->assertDatabaseMissing('messages', ['email' => $exampleMessage['email']]);
    }

    public function testWithoutMessage()
    {
        $exampleMessage = $this->getExampleMessageData();
        $exampleMessage['message'] = null;

        $response = $this->post('/send', $exampleMessage);

        $response->assertSessionHasErrors(['message']);
        $response->assertSessionMissing('status');
        $response->assertRedirect('/');

        Mail::assertNotSent(MessageReceived::class);
        $this->assertDatabaseMissing('messages', ['email' => $exampleMessage['email']]);
    }
}